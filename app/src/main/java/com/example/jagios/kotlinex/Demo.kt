package com.example.jagios.kotlinex

import javax.security.auth.callback.Callback

//variable
var num = 2

//constante (inmutable)
val pi = 3.141516

var edad:Int = 28
var nombre:String = "Gus"

//variable Opcional (puede tener un valor o ser Nula)
var apellido:String? = ""

    //en java toca validar
    /*fun ejemplo(){
        if(apellido != null){
            var sub = apellido.substring(0,1)
        }
    }*/

    //en kotlin haciendo uso del opcional, en caso de no tener un valor se asigna un nulo
    var subApellido:String? = apellido?.substring(0,1)

    //en kotlin hacinedo el string no es opcional, por lo tanto en caso de no tener un valor se le asigna sin apellido
    var subApellido2:String = apellido?.substring(0,1)?: "Sin Apellido"

lateinit var color:String

// los simbolos !! es non-null asserted (se asegura que el string no es nulo
var subApellido3:String = apellido!!.substring(0,1)


//se inicializa cuando se utiliza la variable, por medio de delegados (by lazy) cuando se utiliza se le asigna el valor (en el ejemplo 123)
//val id:String by lazy(123)


//FUNCIONES
fun operar(n1:Int, n2:Int):Int{
    return n1+n2
}

fun sumar2(n1:Int, n2:Int = 0):Int = n1+n2


var rta = sumar2(3,5)
//como se tiene un valor por defecto, no es necesario mandar el parametro ya que esta por defecto
var rta2 = sumar2(3)
//se puede eniar los valores con los nombres de los parametros, en este caso estan en desorden pero los recibe de igual forma
var rta3 = sumar2(n2 = 6, n1 = 7)


fun Int.mul(n:Int):Int = this * n

var rta4 = 4.mul(6)

// Crear operador
infix fun Int.div(n:Int):Int = this/n
var rta5 = 4 div 2

var sub: (n1:Int, n2:Int)->Int = {n1,n2 -> n1 - n2}
var rta6 = sub(7,6)

// Callbacks
fun operar(n1:Int, n2:Int, callback: (rta:Int)->Unit){
    callback(n1+n2)
}

fun testOperar(){
    operar(1,2,{rta-> println("resultado: $rta")})

    operar(2,3){rta->println("resultado: rta")}

    operar(1,7){ println("resultado: $it")}
}


// CLASES
class Usuario(val nombre:String, var email:String)

var usr = Usuario("Gus","gmoran@unicauca.edu.co")

val n = usr.nombre

fun testUsuario(){
    usr.email = "otro@email.com"
}


open class Mascota(val edad:Int, val raza:String){
    var edadHumana:Int=0

    init {
        edadHumana = 7 * edad
    }
}

//constructor secundario
class Conejo(val nombre:String, edad:Int):Mascota(edad,"Conejo"){

    constructor():this("",0)

    fun comer(comida:Int){

    }
    //sobreescribir un operador (sobrecarga de operadores)
    operator fun plus(conejo:Conejo):Conejo{
        return Conejo(conejo.nombre, this.edad)
    }

}

val a = Conejo("a",9)
val b = Conejo("b",2)
val c = a + b


// BIFURCACIONES

fun testif(edad: Int) {
    val mayor = if (edad < 18) false else true

    when (edad) {
        // el extremo es inlcuyente
        in 0..10 -> println("niño")
        in 11..17 -> println("joven")
        in 18..30 -> println("adulto joven")

        // no se incluye el extremo
        in 31 until 40 -> println("Adulto")
    }

    var obj:Any = 10

    when (obj) {
        is Int -> obj + 10
        is String -> obj.substring(10)
    }

    val menor = when(edad){
        in 0..17 -> "menor"
        else -> "mayor"
    }

    for(i in 0..10){

    }

    for(i in 0 until 10){

    }

    val frutas = listOf("Manzana", "Pera", "Tomate")
    for((index,fruta) in frutas.withIndex()) {

    }

}



// ARREGLOS Y TUPLAS

val color2:Pair<Int, String> = Pair(22,"rojo")
val color3:Pair<Int, String> = 22 to "Rojo"

//lista inmutable
val colores:List<Int> = listOf(12312, 312312312,312312)

//lista mutable
val coloresMu:MutableList<Int> = mutableListOf(123,131232,13123)

